#!/bin/sh

#passwd=`cat config.txt | sed '/^PASSWD=/!d;s/.*=//'`
worker_ip=`cat ip.txt |grep "^[1-9]" | awk '{print $1}'`
username=root

#check the network of worker node
checkNetwork()
{
    echo "check network..."
    flag=0
    for i in $worker_ip
    do
        ping -c 1 $i >/dev/null
        if [ $? -eq 0 ];then
            echo "$i: The network is ok."
        else
            echo "$i: The network is not ok."
            flag=$(($flag+1))
        fi
    done
    if [ $flag -gt 0 ]; then
        echo "pls check your network."
        exit 1
    fi
}

establishTrust()
{
    echo "Check password free login..."
    if [ ! -f ~/.ssh/id_rsa.pub -o ! -f ~/.ssh/id_rsa.pub ]
    then
        expect -c "
        spawn ssh-keygen -t rsa
        expect {
            \"*.ssh/id_rsa*\" {send \"\r\";exp_continue}
            \"*passphrase*\" {send \"\r\";exp_continue}
            \"*passphrase again*\" {send \"\r\";}
         }"

    else
        echo "The id_rsa.pub has been generated."
    fi

    for ip in $worker_ip
    do
        ssh -o PreferredAuthentications=publickey -o StrictHostKeyChecking=no $username@$ip date >/dev/null 2>&1
        if [ $? -eq 0 ]
        then
            echo "$ip: Have established trust among the nodes..."
        else
            IFS_OLD=$IFS
            IFS=
            read -p "pls input password for establist trust $ip:" -s password
            IFS=$IFS_OLD
            expect -c "
            spawn ssh-copy-id $username@$ip
            expect {
　　            \"*yes/no*\" {send \"yes\r\"; exp_continue}
　　            \"*assword:\" {send \"$password\r\"; exp_continue}
                \"*ermission denied*\" {send \"123456789\r\";}
　　        }"
        fi

    done
}

###### Check if config file exist #####
judgeResult()
{
    if [ $? -ne 0 ];then
        echo "$1: $2 not exist."
    fi
}

checkConfigFile()
{
    while read line
    do
        ip=`echo $line |awk '{print $1}'`
        etc_path=`echo $line |awk '{print $2}'`
        config_properties_file=`ssh root@$ip "ls $etc_path/config.properties" </dev/null`
        judgeResult $ip "config.properties"
        jvm_config_file=`ssh root@$ip "ls $etc_path/jvm.config" </dev/null`
        judgeResult $ip "jvm.config"
        state_store_file=`ssh root@$ip "ls $etc_path/state-store.properties" </dev/null`
        judgeResult $ip "state-store.properties"
        hive_properties_file=`ssh root@$ip "ls $etc_path/catalog/hive*.properties" </dev/null`
        judgeResult $ip "hive.properties"
    done < ip.txt
}

###### Get Recommended mem #####
getExceptConfig()
{
    
    array=()
    flag=0
    while read line
    do
        ip=`echo $line |awk '{print $1}'`
        etc_path=`echo $line |awk '{print $2}'`
        avai_mem_num_01=`ssh root@$ip "free -h |grep -i mem" </dev/null`
        avai_mem_num_02=`echo $avai_mem_num_01 |awk '{print $NF}' |tr -cd [0-9]`
        array[$flag]=$avai_mem_num_02
        flag=$(($flag+1))
    done < ip.txt
    
    #Get the min avai_mem among all node
    let min=${array[0]}
    
    for (( i=0;i<${#array[*]};i++))
    do
        [[ ${min} -gt ${array[$i]} ]] && min=${array[$i]}
    done
    avai_mem_num=$min

    #Get except config
    node_num=`cat ip.txt |grep worker |wc -l`
    avai_mem_unit=`free -h |grep -i mem |awk '{print $NF}' |tr -cd "[A-Z]"`
    jvm_mem=`echo "$avai_mem_num*0.8" |bc |awk -F'.' '{print $1}'`
    mem_per_node=`echo "$jvm_mem*0.65" |bc |awk -F'.' '{print $1}'`
    mem_total=`echo "$mem_per_node*$node_num" |bc |awk -F'.' '{print $1}'`
    mem_heap=`echo "$jvm_mem*0.3" |bc |awk -F'.' '{print $1}'`
}

judgement()
{
    if [ $# -eq 2 ];then
        echo -e "\033[31m Suggest add config to $2 \033[0m"
    elif [ "$1" != "$2" ];then
        echo -e "\033[31m Suggest change config to $3 \033[0m"
    else
        echo -e "\033[32m $3 check pass. \033[0m"
    fi
}

judgement_str()
{
    if [ $# -eq 2 ];then
        echo -e "\033[32m $2 check pass. \033[0m"
    elif [ "$1" = "$2" ];then
        echo -e "\033[32m $3 check pass. \033[0m"
    else
        echo -e "\033[31m suggest change config to $3 or not to set this item. \033[0m"
    fi
}

##### check config/jvm/hive file #####
checkConfigProperties()
{
    while read line
    do
        #collect the config info from every node 
        ip=`echo $line |awk '{print $1}'`
        etc_path=`echo $line |awk '{print $2}'`
        echo 
        echo "check result for ip:$ip"
        echo "--------------------------------------------"
        #check jvm
        echo "jvm.config:"
        Xmx=`ssh root@$ip "sed -n '/^-Xmx/p' $etc_path/jvm.config" </dev/null |tr -cd [0-9]`
        judgement $Xmx $jvm_mem "-Xmx$jvm_mem$avai_mem_unit"
        agentlib=`ssh root@$ip "cat $etc_path/jvm.config |grep ^-agentlib:jdwp" </dev/null`
        if [ $? -eq 0 ];then
            echo -e "\033[31m Suggest removing below config in jvm.comfig \033[0m"
            for j in $agentlib;do
               echo -e "\033[31m $j \033[0m"
            done
        else
            echo -e "\033[32m -agentlib:jdwp=transport=dt_socket check pass. \033[0m"
        fi
        jmxremote=`ssh root@$ip "cat $etc_path/jvm.config |grep ^-Dcom.sun.management.jmxremote" </dev/null`
        if [ $? -eq 0 ];then
            echo -e "\033[31m Suggest removing below config in jvm.comfig \033[0m"
            for j in $jmxremote;do
               echo -e "\033[31m $j \033[0m"
            done
        else
            echo -e "\033[32m -Dcom.sun.management.jmxremote check pass. \033[0m"
        fi
        
        #config.properties
        echo
        echo "config.properties:"
        query_max_memory=`ssh root@$ip "sed -n '/^query.max-memory=/p' $etc_path/config.properties" </dev/null |tr -cd [0-9]`
        judgement $query_max_memory $mem_total "query.max-memory=$mem_total${avai_mem_unit}B"

        query_max_total_memory=`ssh root@$ip "sed -n '/^query.max-total-memory=/p' $etc_path/config.properties" </dev/null |tr -cd [0-9]`
        judgement $query_max_total_memory $mem_total "query.max-total-memory=$mem_total${avai_mem_unit}B"
        
        query_max_memory_per_node=`ssh root@$ip "sed -n '/^query.max-memory-per-node/p' $etc_path/config.properties" </dev/null |tr -cd [0-9]`
        judgement $query_max_memory_per_node $mem_per_node "query.max-memory-per-node=$mem_per_node${avai_mem_unit}B"

        query_max_total_memory_per_node=`ssh root@$ip "sed -n '/^query.max-total-memory-per-node/p' $etc_path/config.properties" </dev/null |tr -cd [0-9]`
        judgement $query_max_total_memory_per_node $mem_per_node "query.max-memory-per-node=$mem_per_node${avai_mem_unit}B"
       
        memory_heap=`ssh root@$ip "sed -n '/^memory.heap-headroom-per-node/p' $etc_path/config.properties" </dev/null |tr -cd [0-9]`
        judgement $memory_heap $mem_heap "query.max-memory-per-node=$mem_per_node${avai_mem_unit}B"
        
        dynamic_filtering=`ssh root@$ip "sed -n '/^enable-dynamic-filtering/p' $etc_path/config.properties" </dev/null |awk -F'=' '{print $2}' |tr -cd [a-z]`
        judgement_str $dynamic_filtering "true" "enable-dynamic-filtering=true"
 
        enable_execution_plan_cache=`ssh root@$ip "sed -n '/^experimental.enable-execution-plan-cache/p' $etc_path/config.properties" </dev/null |awk -F'=' '{print $2}' |tr -cd [a-z]`
        judgement_str $enable_execution_plan_cache "true" "experimental.enable-execution-plan-cache=true"
        
        #state_store=`ssh root@$ip "sed -n '/^hetu.embedded-state-store.enabled/p' $etc_path/config.properties" </dev/null |awk -F'=' '{print $2}' |tr -cd [a-z]`
        #judgement $state_store "true" "hetu.embedded-state-store.enabled=true"
        
        hetu_executionplan_cache=`ssh root@$ip "sed -n '/^hetu.executionplan.cache.enabled/p' $etc_path/config.properties" </dev/null |awk -F'=' '{print $2}' |tr -cd [a-z]`
        judgement_str $hetu_executionplan_cache "true" "hetu.executionplan.cache.enabled=true"
      
        echo -e "\033[33m For 'task.concurrency' in 'config.properties' \033[0m" 
        task_concurrency=`ssh root@$ip "grep ^task.concurrency $etc_path/config.properties" </dev/null`
        if [ $? -eq 0 ];then
            echo -e "\033[33m Your config:$task_concurrency \033[0m"
        else
            echo -e "\033[33m There no set 'task_concurrency'(this config default values is 'task.concurrency=16') \033[0m"
        fi
        echo -e "\033[33m When excute 1 concurrency, suggest to set task.concurrency=16 or not to set this config(this config default values is 16) \033[0m"
        echo -e "\033[33m When excute high concurrency, suggest to set task.concurrency=4 \033[0m"

        #hive.properties
        echo 
        echo "hive.properties"
        hive_file=`ssh root@$ip "find ${etc_path}/catalog/ -name hive*properties" </dev/null`
        for hf in $hive_file;do
            echo "for $hf:"
            hive_dynamic_filter=`ssh root@$ip "sed -n '/^hive.dynamic-filter-partition-filtering/p' $hf" </dev/null |awk -F'=' '{print $2}' |tr -cd [a-z]`
            if [ x"$hive_dynamic_filter" = x"" ];then
                hive_dynamic_filter="pass"
            fi
            judgement_str $hive_dynamic_filter "true" "hive.dynamic-filter-partition-filtering=true"
        done
        echo "--------------------------------------------" 
        
        
        done <ip.txt
}

checkNetwork
establishTrust
checkConfigFile
getExceptConfig
checkConfigProperties

