#!/bin/sh

echo "+++++ Execute establishTrustAmongNodes +++++"
cn_ip=`cat config.txt | sed '/^CN_IP=/!d;s/.*=//'`

establishTrustAmongNodes()
{
    scp_tmp_dir="/tmp/openlookeng_install_tmp"
    for i in $cn_ip
    do
        ssh root@$i "[ -d $scp_tmp_dir ] && echo ok || mkdir -p $scp_tmp_dir" >/dev/null
        scp config.txt establish_trust.sh root@$i:$scp_tmp_dir >/dev/null 2>&1
        ssh root@$i "cd $scp_tmp_dir && sh establish_trust.sh && rm -rf $scp_tmp_dir"
    done
}

establishTrustAmongNodes
