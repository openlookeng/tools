#!/bin/sh

#This script path
current_relative_path=`dirname $0`
current_path=`cd $current_relative_path;pwd`

#openlookeng install path
install_dir_path=`cat $current_path/config.txt |sed '/^INSTALL_DIR=/!d;s/.*=//'`
install_dir=${install_dir_path}/openlookeng
if [ "$install_dir" = "" ];then
    echo "Must define INSTALL_DIR in config.txt. Exit..."
    exit 1
fi

#get cn & worker ip from ip.txt
cn_ip=`cat $current_path/config.txt |sed '/^CN_IP=/!d;s/.*=//'`
worker_ip=`cat $current_path/config.txt |sed '/^WORKER_IP=/!d;s/.*=//'`
all_ip=`echo "$cn_ip $worker_ip"`

#check the network of worker node
checkNetwork()
{   
    flag=0
    for i in $worker_ip
    do
        ping -c 1 $i >/dev/null
        if [ $? -eq 0 ];then
            echo "$i: The network is ok."
        else
            echo "$i: The network is not ok."
            flag=$(($flag+1))
        fi
    done
    if [ $flag -gt 0 ]; then
        echo "pls check your network."
        exit 1
    fi
}

#check if there is install package
checkPackage()
{
    server_package=`ls $current_path |grep "hetu-server.*gz"`
    if [ $? -eq 0 ]
    then
        s_package_num=`echo $server_package |wc -w`
        if [ $s_package_num -gt 1 ]
        then
            echo $server_package
            echo "The server package more than 1"
            exit 1
        fi
    else
        echo "There is no server package found, exit..."
        exit 1
    fi
}

#check Openlookeng service and shutdown
checkService()
{
    #kill openlookeng process on all node
    flag=1
    for ip in $all_ip
    do
        work_process=`ssh root@$ip "ps -ef |grep hetu |grep -v grep" |awk '{print $2}' |xargs`
        if [ ! -n "$work_process" ]
        then
            echo "$ip: there is no openlookeng service."
            continue
        else
            if [ $flag -eq 1 ]
            then
                read -r -p "The openlookeng service has been launch, need to stop it.[y/n]" input
                case $input in
                y)
                echo "$ip: kill the openlookeng service..."
                ssh root@$ip "kill -9 $work_process"
                ;;
                *)
                echo "Exit..."
                exit 1
                ;;
                esac
                ((flag++))
            else
                echo "$ip: kill the openlookeng service..."
                ssh root@$ip "kill -9 $work_process"
            fi
        fi
    done
}

#install the openlookeng
installOpenLK()
{
    #install service
    #check if there is openlookeng install dir on CN node first
    exist_list=()
    for ip in $all_ip
    do
        echo "+++++ $ip installing +++++"
        ssh root@$ip "ls $install_dir" >/dev/null 2>&1
        if [ $? -eq 0 ]
        then
            exist_list+=($ip)
        fi
    done
    list=`echo ${exist_list[*]}`
    if [ x"$list" != x ];then
        echo "${exist_list[*]} $install_dir already exist"
        read -r -p "Do you want to remove this dir?[y/n]" input
        case $input in
            y)
                echo "Removing $install_dir ..."
                for i in ${exist_list[*]};do
                    ssh root@$i rm -rf $install_dir
                done
                ;;
            n)
                echo "Exit..."
                exit 1
                ;;
        esac
     fi
   
    #create openlookeng path
    for ip in $all_ip
    do
        echo "$ip: create install dir on the node"
        ssh root@$ip "mkdir -p $install_dir"
        scp -r $current_path/*.gz root@$ip:$install_dir
        ssh root@$ip "cd $install_dir;tar -xf *.tar.gz"
        if [ $? -ne 0 ]; then
            echo ”tar -xf command on $ip failed. Exit..“
            exit 1
        fi
    done
    
} 

#generate the start & stop script
genLaunchScript()
{
    h_path=`find $install_dir -maxdepth 1 -type d -name "hetu*"`
    sed -i "/^ip=/s/\".*\"/\"$all_ip\"/g" $current_path/launch/start.sh
    sed -i "/^cn_ip=/s/\".*\"/\"$cn_ip\"/g" $current_path/launch/scp_file.sh
    sed -i "/^worker_ip=/s/\".*\"/\"$worker_ip\"/g" $current_path/launch/scp_file.sh
    sed -i "/^hetu_path=/s#\".*\"#\"$h_path/bin/launcher\"#g" $current_path/launch/start.sh
    sed -i "/^scp_path=/s#\".*\"#\"$h_path/etc/\"#g" $current_path/launch/scp_file.sh
    sed -i "/^ip=/s/\".*\"/\"$all_ip\"/g" $current_path/launch/stop.sh
    sed -i "/^hetu_path=/s#\".*\"#\"$h_path/bin/launcher\"#g" $current_path/launch/stop.sh
    cp $current_path/launch/start.sh $install_dir
    cp $current_path/launch/stop.sh $install_dir
    cp $current_path/launch/scp_file.sh $install_dir
    cp -r $current_path/etc_cn $install_dir
    cp -r $current_path/etc_worker $install_dir
}

echo "+++++ Excuting install.sh +++++"
checkNetwork
checkService
checkPackage
installOpenLK
genLaunchScript
