#!/bin/sh

#### 同步 etc_cn/ 和 etc_worker/ 下的文件到CN和Worker节点 ####

cn_ip="127.0.0.1 127.0.0.2"
worker_ip="127.0.0.3 127.0.0.4"
scp_path="/opt/130_testinstall/openlookeng/hetu-server-1.3.0-SNAPSHOT/etc/"



for i in $cn_ip
do
    sed -i "/^discovery.uri=/s#\(discovery.uri=http://\).*\(:.*\)#\1$i\2#g"  ./etc_cn/config.properties
    scp -r ./etc_cn/* root@$i:$scp_path
done

for j in $worker_ip
do
        scp -r ./etc_worker/* root@$j:$scp_path
done
