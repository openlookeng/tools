#!/bin/sh

#### 启动集群所有节点 ####

ip="127.0.0.1 127.0.0.2 127.0.0.3"
hetu_path="/opt/130_testinstall/openlookeng/hetu-server-1.3.0-SNAPSHOT/bin/launcher"

source /etc/profile

for i in $ip
do
        ssh root@$i "source /etc/profile;$hetu_path start"
done
