#!/bin/sh

judgeScript()
{
    if [ $? -ne 0 ]; then
        echo ”$1 failed. Exit..“
        exit 1
    else
        echo "$1 pass."
    fi
}



sh establish_trust.sh
judgeScript "establish_trust.sh"

sh establish_trust_among_nodes.sh
judgeScript "establish_trust_among_nodes.sh"

sh install.sh
judgeScript "install.sh"

sh setting.sh
judgeScript "setting.sh"
