#### 1.The script explanation

establish_trust.sh - establish trust between CN node and worker nodes to free password login
establish_trust.sh usage: Enter the current path and excute "./establish_trust.sh"
install.sh - distribute the tar.gz package to each node (We need to put the installation tar.gz package in the current directory and remove the tar.gz which does needed)
install.sh usage: Enter the current path and excute "./install.sh"
setting.sh - automatically configure /etc files for each node
setting.sh usage: Enter the current path and excute "./setting.sh"
main.sh - excute install.sh & setting.sh & establish_trust.sh
main.sh usage: Enter the current path and excute "./main.sh"

=======================================================================================

#### 2.config.txt explanation

Below is the content of config.txt and we need to customize these configurations. 
###Separated by spaces among WORKER_IP ip###

```
CN_IP=127.0.0.1 127.0.0.2
WORKER_IP=127.0.0.3 127.0.0.4
INSTALL_DIR=/opt/tmp                    --Define openlookeng installation directory
core_site=/opt/hetu/core-site.xml       --Define the path of  HDFS config file 
hdfs_site=/opt/hetu/hdfs-site.xml
```



##### config.properties #####
```
discovery.uri=http://127.0.0.1:8090        --Define the discovery.uri in config.properties file
experimental.spiller-spill-path=/srv/BigData/hadoop/data1/hetu-core
```



##### hive.properties #####
```
hive.metastore.uri=thrift://127.0.0.1:21088,thrift://127.0.0.2:21088    --Define the hive.metastore.uri in hive.properties file
hive.config.resources=/opt/hetu/conf/core-site.xml,/opt/hetu/conf/hdfs-site.xml -- --Define the path of  HDFS config file
```



##### hetu-metastore.properties #####
Configuring hetu-metastore when need to use hetu-metastore.
If you need not hetu-metastore, you can remove this part of parameters

##### state-store.properties #####
Configuring state-store parameters.
If you need not state-store, you can remove this part of parameters

=======================================================================================

#### 3.etc_cn & etc_worker

etc_cn: this dir include the setting of CN node, you also can customize these setting if need.
etc_worker: this dir include the setting of worker node, you also can customize these setting if need.
The ./setting.sh will distribute the 2 directorys to CN and worker node

=======================================================================================

#### 4.launch

The dir include the script "start/stop/scp_file" and the ./install.sh will cp them to CN node
start.sh: start the cluster
stop.sh: stop the cluster
scp_file.sh: scp the files under etc_cn/etc_worker to CN/Worker node

=======================================================================================

#### 5.check_setting.sh

The script is to check the jvm.config/config.properties/hive.properties
First, need to set the ip.txt file as below:

```
cat ip.txt
127.0.0.1 /opt/openlookeng/hetu-server-1.0.0-SNAPSHOT/etc/ cn
127.0.0.2 /opt/openlookeng/hetu-server-1.0.0-SNAPSHOT/etc/ worker
```

The 1st column is IP, the 2nd column is the path to check and the 3rd column is the role of the node
usage: enter the current directory and excute it(./check_setting.sh)

The 1st column is IP, the 2nd column is the path to check and the 3rd column is the role of the node
usage: enter the current directory and excute it(./check_setting.sh)